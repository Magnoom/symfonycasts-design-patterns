# Design Patterns for Fun and Proficiency in Symfony! 

Well hi there! This repository holds the code and script
for the [Design Patterns for Fun and Proficiency Tutorials](https://symfonycasts.com/screencast/design-patterns) on SymfonyCasts.

## Setup

If you've just downloaded the code, congratulations!!

To get it working, follow these steps:

**Download Composer dependencies**

Make sure you have [Composer installed](https://getcomposer.org/download/)
and then run:

```
composer install
```

You may alternatively need to run `php composer.phar install`, depending
on how you installed Composer.

**Running the App!**

This is a command-line only app - so no web server needed. Instead, run:

```
php bin/console app:game:play
```

Have fun and battle hard!

## Have Ideas, Feedback or an Issue?

If you have suggestions or questions, please feel free to
open an issue on this repository or comment on the course
itself. We're watching both :).

## Thanks!

And as always, thanks so much for your support and letting
us do what we love!

<3 Your friends at SymfonyCasts

// -------------------------------------- //
# CODING
// -------------------------------------- //

## 01. Design Patterns & Their Types

**These many design patterns fall into three basic groups:**

- `creational patterns`. Helps instantiate objects.
  - `factory pattern`,
  - `builder pattern`,
  - `singleton pattern`...
- `structural patterns`. Help organize relationships between objects.
One example of a relationship would be a parent-child relationship, but there are many others.
  - `decorator pattern`,
  - `composite pattern`...
- `behavioral patterns`. Helps solve communication problems between objects. Assigning responsibilities between objects.
We can say that behavioral patterns help you design classes with specific responsibilities that can then work together... instead of putting all of that code into one giant class.
  - `strategy pattern`,
  - `observer pattern`...

## 02. Strategy Pattern

**This is a `behavioral pattern` that helps organize code into separate classes that can then interact with each other.**

> The strategy pattern defines a family of algorithms, encapsulates each one and makes them interchangeable. It lets the algorithm vary independently of clients that use it.

If that made sense to you, congrats! You get to teach the rest of the tutorial! Let's try that again. Here's my definition:

> The strategy pattern is a way to allow part of a class to be rewritten from the outside.

The `Strategy Pattern` is useful when you need to change the behavior (the code) of an algorithm (a method inside a class) without touching its implementation.

## 03. Strategy Part 2: Benefits & In the Wild

### Another Strategy Pattern Example

1. *Step one is to identify the code that needs to change and create an interface for it.*

    - Create new `ArmorType` interface: `ArmorType/ArmorType.php`

        ```php
        [ArmorType.php]
        interface ArmorType
        {
            public function getArmorReduction(int $damage): int;
        }
        ```

2. *Step two is to create at least one implementation of this.*

    - Create new class `ShieldType`: `ArmorType/ShieldType.php`. Implement `ArmorType` interface. Write logic.
    
        ```php
        [ShieldType.php]
        class ShieldType implements ArmorType
        {
            public function getArmorReduction(int $damage): int
            {
                $chanceToBlock = Dice::roll(100);
    
                return $chanceToBlock > 80 ? $damage : 0;
            }
        }
        ```
    
    - Create new class `LeatherArmorType`: `ArmorType/LeatherArmorType.php`. Implement `ArmorType` interface. Write logic.
    
        ```php
        [LeatherArmorType.php]
        class LeatherArmorType implements ArmorType
        {
            /**
             * Absorbs 25% of the damage
             */
            public function getArmorReduction(int $damage): int
            {
                return floor($damage * 0.25);
            }
        }
        ```
      
    - Create new class `IceBlockType`: `ArmorType/IceBlockType.php`. Implement `ArmorType` interface. Write logic.
    
        ```php
        [IceBlockType.php]
        class IceBlockType implements ArmorType
        {
            /**
             * Absorbs 2d8
             */
            public function getArmorReduction(int $damage): int
            {
                return Dice::roll(8) + Dice::roll(8);
            }
        }
        ```

3. *Ok step three: allow an object of the ArmorType interface to be passed into Character... then use its logic.*

    ```php
    [Character.php]
    class Character
    {
        public function __construct(
            private int $maxHealth,
            private int $baseDamage,
            private AttackType $attackType,
            private ArmorType $armorType
        ) {
            $this->currentHealth = $this->maxHealth;
        }
    
        ...
    
        public function receiveAttack(int $damage): int
        {
            $armorReduction = $this->armorType->getArmorReduction($damage);
            $damageTaken = max($damage - $armorReduction, 0);
            $this->currentHealth -= $damageTaken;
    
            return $damageTaken;
        }
    
        ...
   
    }
    ```
   
    ```php
    [GameApplication.php]
    class GameApplication
    {
        ...
   
        public function createCharacter(string $character): Character
        {
            return match (strtolower($character)) {
                'fighter' => new Character(90, 12, new TwoHandedSwordType(), new ShieldType()),
                'archer' => new Character(80, 10, new BowType(), new LeatherArmorType()),
                'mage' => new Character(70, 8, new FireBoltType(), new IceBlockType()),
                'mage_archer' => new Character(75, 9, new MultiAttackType([new BowType(), new FireBoltType()]), new ShieldType()),
                default => throw new \RuntimeException('Undefined Character'),
            };
        }
        
        ...
   
    }
    ```

### Pattern Benefits

- Unlike inheritance, we can now create characters with endless combinations of attack and armor behaviors.
- We could also swap out an `AttackType` or `ArmorType` at runtime. This means that we could, for example, read some configuration or environment variable and dynamically use it to change one of the attack types of our characters on the fly. That's not possible with inheritance.

### Pattern and SOLID Principle
*The Strategy Pattern is a clear win for `SRP` - the `single responsibility principle` - and `OCP` - the `open closed principle`.*
- `SRP`: The Strategy Pattern allows us to break big classes like `Character` into smaller, more focused ones, but still have them interact with each other.
- `OCP`: Now have a way to modify or extend the behavior of the `Character` class without actually changing the code inside. We can pass in new armor and attack types instead.

## 04. Builder Pattern

*This is one of those creational patterns that help you instantiate and configure objects. And, it's a bit easier to understand than the `strategy pattern`.*

The official definition of the "builder pattern" is this:

> A creational design pattern that lets you build and configure complex objects step-by-step.

That... actually made sense. Part two of the official definition says:

> the pattern allows you to produce different types and representations of an object using the same construction code.

*In other words, you create a builder class that helps build other objects... and those object might be of different classes or the same class with different data.*

- Create a new class `CharacterBuilder`: `Builder/CharacterBuilder.php`

    ```php
    [CharacterBuilder.php]
    class CharacterBuilder
    {
        public const ATTACK_TYPE_BOW = 'bow';
        public const ATTACK_TYPE_FIRE_BOLT = 'fire_bolt';
        public const ATTACK_TYPE_SWORD = 'sword';
    
        public const ARMOR_TYPE_ICE_BLOCK = 'ice_block';
        public const ARMOR_TYPE_LEATHER_ARMOR = 'leather_armor';
        public const ARMOR_TYPE_SHIELD = 'shield';
    
        private int $maxHealth;
        private int $baseDamage;
        private string $attackType;
        private string $armorType;
        
        public function setMaxHealth(int $maxHealth): self
        {
            $this->maxHealth = $maxHealth;
    
            return $this;
        }
    
        public function setBaseDamage(int $baseDamage): self
        {
            $this->baseDamage = $baseDamage;
    
            return $this;
        }
    
        public function setAttackType(string $attackType): self
        {
            $this->attackType = $attackType;
    
            return $this;
        }
    
        public function setArmorType(string $armorType): self
        {
            $this->armorType = $armorType;
    
            return $this;
        }
    
        public function buildCharacter(): Character
        {
            return new Character(
                $this->maxHealth,
                $this->baseDamage,
                $this->createAttackType(),
                $this->createArmorType(),
            );
        }
    
        private function createAttackType(): AttackType
        {
            return match ($this->attackType) {
                self::ATTACK_TYPE_BOW => new BowType(),
                self::ATTACK_TYPE_FIRE_BOLT => new FireBoltType(),
                self::ATTACK_TYPE_SWORD => new TwoHandedSwordType(),
                default => throw new \RuntimeException('Invalid attack type given')
            };
        }
    
        private function createArmorType(): ArmorType
        {
            return match ($this->armorType) {
                self::ARMOR_TYPE_ICE_BLOCK => new IceBlockType(),
                self::ARMOR_TYPE_SHIELD => new ShieldType(),
                self::ARMOR_TYPE_LEATHER_ARMOR => new LeatherArmorType(),
                default => throw new \RuntimeException('Invalid armor type given')
            };
        }
    }
    ```

The methods in `Builder pattern` return `self`: it will return `CharacterBuilder`. This is really common in the `builder pattern` because it allows method chaining, also known as a `fluent interface`. But, it's not a requirement of the pattern.

The `builder pattern` is very good at hiding the instantiation details of your objects. It allows you to produce different types (e.g. potentially different classes) and representations (e.g. different data) of an object using the same construction code.

## 05. Builder Improvements

**Clearing State After Building?**
One more thing about the builder class! In the "build" method, after you create the object, you may choose to "reset" the builder object. For example, we could set the Character to a variable, then, before we return it, reset $maxHealth and all the other properties back to their original state. Why would we do this? Because it would allow for a single builder to be used over and over again to create many objects - or, characters in this case.
We are not going to use this right now.

**So, use the Builder**

- At first create new method `createCharacterBuilder` in `GameApplication.php`:

    ```php
    [GameApplication.php]
    class GameApplication
    {
        ...
        
        private function createCharacterBuilder(): CharacterBuilder
        {
            return new CharacterBuilder();
        }
    }
    ```

- Change the return of `createCharacter()` method:

    ```php
    [GameApplication.php]
    class GameApplication
    {
        ...
        
        public function createCharacter(string $character): Character
        {
            return match (strtolower($character)) {
                'fighter' => $this->createCharacterBuilder()
                    ->setMaxHealth(90)
                    ->setBaseDamage(12)
                    ->setAttackType(CharacterBuilder::ATTACK_TYPE_SWORD)
                    ->setArmorType(CharacterBuilder::ARMOR_TYPE_SHIELD)
                    ->buildCharacter(),
                'archer' => $this->createCharacterBuilder()
                    ->setMaxHealth(80)
                    ->setBaseDamage(10)
                    ->setAttackType(CharacterBuilder::ATTACK_TYPE_BOW)
                    ->setArmorType(CharacterBuilder::ARMOR_TYPE_LEATHER_ARMOR)
                    ->buildCharacter(),
                'mage' => $this->createCharacterBuilder()
                    ->setMaxHealth(70)
                    ->setBaseDamage(8)
                    ->setAttackType(CharacterBuilder::ATTACK_TYPE_FIRE_BOLT)
                    ->setArmorType(CharacterBuilder::ARMOR_TYPE_ICE_BLOCK)
                    ->buildCharacter(),
                'mage_archer' => $this->createCharacterBuilder()
                    ->setMaxHealth(75)
                    ->setBaseDamage(9)
                    ->setAttackType(CharacterBuilder::ATTACK_TYPE_FIRE_BOLT)
                    ->setArmorType(CharacterBuilder::ARMOR_TYPE_SHIELD)
                    ->buildCharacter(),
                default => throw new \RuntimeException('Undefined Character'),
            };
        }
        
        ...
        
    }
    ```

### Allow for Multiple Attack Types

So what about allowing our mage_archer's two attack types? Well, that's the beauty of the builder pattern. Part of our job, when we create the builder class, is to make life as easy as possible for whoever uses this class. That's why I chose to use string `$armorType` and `$attackType` instead of objects.

```php
[GameApplication.php]
class GameApplication
{
    ...
    
    public function createCharacter(string $character): Character
    {
        return match (strtolower($character)) {
            
            ...
            
            'mage_archer' => $this->createCharacterBuilder()
                ->setMaxHealth(75)
                ->setBaseDamage(9)
                ->setAttackType(CharacterBuilder::ATTACK_TYPE_FIRE_BOLT, CharacterBuilder::ATTACK_TYPE_BOW)
                ->setArmorType(CharacterBuilder::ARMOR_TYPE_SHIELD)
                ->buildCharacter(),
            default => throw new \RuntimeException('Undefined Character'),
        };
        
        ...
    }
```

- In `CharacterBuilder`, change the argument to `...$attackTypes`, using the `...` to accept any number of arguments.
- Then, since this will now hold an array, change the property to private `array $attackTypes` and down, `$this->attackTypes = $attackTypes`.
- Then make a few changes in `buildCharacter()` method like changing the $attackTypes strings into objects.

    ```php
    [CharacterBuilder.php]
    class CharacterBuilder
    {
        ...
        
        private array $attackTypes;
        
        ...
    
        public function setAttackType(string ...$attackTypes): self
        {
            $this->attackTypes = $attackTypes;
    
            return $this;
        }
        
        ...
    
        public function buildCharacter(): Character
        {
            $attackTypes = array_map(fn(string $attackType) => $this->createAttackType($attackType), $this->attackTypes);
    
            if (count($attackTypes) === 1) {
                $attackType = $attackTypes[0];
            } else {
                $attackType = new MultiAttackType($attackTypes);
            }
    
            return new Character(
                $this->maxHealth,
                $this->baseDamage,
                $attackType,
                $this->createArmorType(),
            );
        }
        
        ...
    }
    ```

## 06. Builder in Symfony & with a Factory

**What if, in order to instantiate the `Character` objects, `CharacterBuilder` needed to, for example, make a *query* to the database?**

- Create a new class `Builder/CharacterBuilderFactory.php`:

    ```php
    [CharacterBuilderFactory.php]
    class CharacterBuilderFactory
    {
        public function createBuilder(): CharacterBuilder
        {
            return new CharacterBuilder();
        }
    }
    ```

*By the way, there is a pattern called the `factory pattern` is just a class whose job is to create another class.*

**This CharacterBuilderFactory is a service. Even if we need five CharacterBuilder objects in our app, we only need one CharacterBuilderFactory. We'll just call this method on it five times.**

- In `GameApplication`, we can create a `public function __construct()` and autowire `CharacterBuilderFactory $characterBuilderFactory`:
    
    ```php
    [GameApplication.php]
    class GameApplication
    {
        public function __construct(private CharacterBuilderFactory $characterBuilderFactory)
        {
        }
        
        ...
        
        private function createCharacterBuilder(): CharacterBuilder
        {
            return $this->characterBuilderFactory->createBuilder();
        }
    }
    ```

**The nice thing about this factory (and this is really the purpose of the factory pattern in general) is that we have centralized the instantiation of this object.**

- let's require `LoggerInterface $logger`:

    ```php
    [CharacterBuilder.php]
    class CharacterBuilder
    {
        ...
        
        public function __construct(private LoggerInterface $logger)
        {
        }
        
        ...
        
        public function buildCharacter(): Character
        {
            $this->logger->info("Creating a character", [
                'maxHealth' => $this->maxHealth,
                'baseDamage' => $this->baseDamage,
            ]);
    
            ...
        }
        
    }
    ```

- `CharacterBuilder` now requires a `$logger`... but `CharacterBuilder` is not a service that we'll fetch directly from the container. We'll get it via `CharacterBuilderFactory`, which is a service. So autowiring `LoggerInterface` will work here:

    ```php
    [CharacterBuilderFactory.php]
    class CharacterBuilderFactory
    {
        public function __construct(private LoggerInterface $logger)
        {
        }
    
        public function createBuilder(): CharacterBuilder
        {
            return new CharacterBuilder($this->logger);
        }
    }
    ```

*We're seeing some of the benefits of the factory pattern here. Since we've already centralized the instantiation of `CharacterBuilder`, anywhere that needs a `CharacterBuilder`, like `GameApplication`, doesn't need to change at all... even though we just added a constructor argument! `GameApplication` was already offloading the instantiation work to `CharacterBuilderFactory`.*

## 07. The Observer Pattern

### The Definition

> The observer pattern defines a one-to-many dependency between objects so that when one object changes state, all of its dependents are notified and updated automatically.

### Okay, not bad, but let's try my version:

> The observer pattern allows a bunch of objects to be notified by a central object when something happens.

**This is the classic situation where you write some code that needs to be called whenever something else happens. And there are actually two strategies to solve this: the `observer pattern` and the `pub-sub pattern`. We'll talk about both. But first up - the `observer pattern`.**

### Anatomy of Observer
There are two different types of classes that go into creating this pattern. The first is called the `subject`. That's the central `object` that will do some work and then notify other `objects` before or after that work. Those other `objects` are the second type, and they're called `observers`.

This is pretty simple. Each `observer` tells the `subject` that it wants to be notified. Later, the `subject` loops over all of the `observers` and "notifies" them... which means it calls a method on them.

**We're going to introduce levels to the characters. Each time you win a fight, your character will earn some XP. After you've earned enough points, the character will "level up".**

1. Create an interface `Observer/GameObserverInterface.php` that all the observers will implement

    ```php
    [GameObserverInterface.php]
    interface GameObserverInterface
    {
        public function onFightFinished(FightResult $fightResult): void;
    }
    ```

2. We need a way for every observer to subscribe/\[unsubscribe\] to be notified on `GameApplication`.

    ```php
    [GameApplication.php]
    class GameApplication
    {
        /** @var GameObserverInterface[] */
        private array $observers = [];
        
        ...
        
        public function subscribe(GameObserverInterface $observer): void
        {
            if (!in_array($observer, $this->observers, true)) {
                $this->observers[] = $observer;
            }
        }
    
        public function unsubscribe(GameObserverInterface $observer): void
        {
            $key = array_search($observer, $this->observers);
    
            if ($key !== false) {
                unset($this->observers[$key]);
            }
        }
        
        private function finishFightResult(FightResult $fightResult, Character $winner, Character $loser): FightResult
        {
            $fightResult->setWinner($winner);
            $fightResult->setLoser($loser);
    
            $this->notify($fightResult);
    
            return $fightResult;
        }
        
        ...
        
        private function notify(FightResult $fightResult): void
        {
            foreach ($this->observers as $observer) {
                $observer->onFightFinished($fightResult);
            }
        }
    }
    ```

## 08. The Observer Class

1. Add new properties and methods to `Character` class:

    ```php
    [Character.php]
    class Character
    {
        ...
        
        private string $nickname = '';
    
        private int $level = 1;
        private int $xp = 0;
        
        ...
        
        public function getLevel(): int
        {
            return $this->level;
        }
    
        public function addXp(int $xpEarned): int
        {
            $this->xp += $xpEarned;
            
            return $this->xp;
        }
        
        public function getXp(): int
        {
            return $this->xp;
        }
   
        public function levelUp(): void
        {
            // +15% bonus to stat
            $bonus = 1.15;
    
            $this->level++;
            $this->maxHealth = floor($this->maxHealth * $bonus);
            $this->baseDamage = floor($this->baseDamage * $bonus);
    
            // todo: level up attack and armor type
        }
        
        ...
    }
    ```

2. Create `Observer/XpEarnedObserver.php` class:

    ```php
    [XpEarnedObserver.php]
    class XpEarnedObserver implements GameObserverInterface
    {
        public function __construct(private readonly XpCalculator $xpCalculator)
        {
        }
    
        public function onFightFinished(FightResult $fightResult): void
        {
            $winner = $fightResult->getWinner();
            $loser = $fightResult->getLoser();
    
            $this->xpCalculator->addXp($winner, $loser->getLevel());
        }
    }
    ```

3. Create `Service/XpCalculator.php` class to delegate XP calculation.
4. Update `execute()` and `printResult()` methods in `GameCommand.php`

    ```php
    [GameCommand.php]
    class GameCommand extends Command
    {
        ...
        
        protected function execute(InputInterface $input, OutputInterface $output): int
        {
            $xpObserver = new XpEarnedObserver(
                new XpCalculator()
            );
            $this->game->subscribe($xpObserver);
            
            ...
            
        }
        
        ...
        
        private function printResult(FightResult $fightResult, Character $player, SymfonyStyle $io)
        {
            ...
            
            $io->writeln('Damage received: ' . $fightResult->getDamageReceived());
            $io->writeln('XP: '. $player->getXp());
            $io->writeln('Final level: '. $player->getLevel());
            $io->writeln('Exhausted Turns: ' . $fightResult->getExhaustedTurns());
            
            ...
        }
    }
    ```

## 09. Observer Inside Symfony + Benefits

We've implemented the `Observer Pattern`! But... that isn't very Symfony-like.

```php
class GameCommand extends Command
{
    ...
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $xpObserver = new XpEarnedObserver(
            new XpCalculator()
        );
        $this->game->subscribe($xpObserver);
   ...
    }
   ...
}
```

Both `XpEarnedObserver` and `XpCalculator` are services. So we would normally autowire them from the container, not instantiate them manually.

1. Remove all of the manual code inside of `GameCommand`. We're going to recreate this same setup inside `services.yaml`.
    ```yaml
   [services.yaml]
    services:
    ...
    autoconfigure: true # Automatically registers your services as commands, event subscribers, etc.
        ...
    
        App\GameApplication:
            calls:
                - subscribe: ['@App\Observer\XpEarnedObserver']
    ...
    ```
    The downside to this solution is that every time we add a new observer, we'll need to go to `services.yaml` and wire it manually.

### Setting up Autoconfiguration

Could we automatically subscribe all services that implement `GameObserverInterface`? Yes! We can do that in two steps.

1. Open `src/Kernel.php`. We're going to override one called `build()`:

    ```php
    [Kernel.php]
    class Kernel extends BaseKernel
    {
        use MicroKernelTrait;
    
        protected function build(ContainerBuilder $container)
        {
            $container->registerForAutoconfiguration(GameObserverInterface::class)
                ->addTag('game.observer');
        }
    
    }
    ```

2. Implement new interface called `CompilerPassInterface`. Then, implement `process()` method. Then remove all of the `yaml` code we added:
    ```php
    [Kernel.php]
    class Kernel extends BaseKernel implements CompilerPassInterface
    {
        use MicroKernelTrait;
    
        protected function build(ContainerBuilder $container)
        {
            $container->registerForAutoconfiguration(GameObserverInterface::class)
                ->addTag('game.observer');
        }
    
        public function process(ContainerBuilder $container)
        {
            $definition = $container->findDefinition(GameApplication::class);
            $taggedObservers = $container->findTaggedServiceIds('game.observer');
            foreach ($taggedObservers as $id => $tags) {
                $definition->addMethodCall('subscribe', [new Reference($id)]);
            }
        }
    }
    ```

    If we need to add another observer later, we can just create a class, make it implement `GameObserverInterface` and... done! It will automatically be subscribed to `GameApplication`.

Sometimes the `observers` are passed in through the constructor! But the idea is always the same: a central object loops over and calls a method on a collection of other objects when something happens.

### Benefits of the Observer Pattern

- This pattern helps the `Single Responsibility` Principle because you can encapsulate (or isolate) code into smaller classes. Instead of putting everything into `GameApplication`.
- This pattern also helps with the `Open-closed` Principle, because we can now extend the behavior of `GameApplication` without modifying its code.
- Follows the `Dependency Inversion` Principle because the high-level class - `GameApplication` - accepts an interface - `GameObserverInterface` - and that interface was designed for the purpose of how `GameApplication` will use it. From `GameApplication's` perspective, this interface represents something that wants to `observe` what happens when something occurs within the game. Namely, the `fight finishing`. And so, `GameObserverInterface` is a good name.

## 10. Publish-Subscriber (PubSub)

It's more of a variation of the `observer pattern`.

### PubSub vs Observer

The key difference between `observer` and `pub/sub` is simply who handles notifying the observers.
- With the `observer pattern`, it's the subject - the thing (like `GameApplication`) that does the work.
- With `pub/sub`, there's a `third object` - usually called a `publisher` - whose only job is to handle this kind of stuff - `event dispatcher`.

With `pub/sub`, the `observers` - `listeners` - tell the `dispatcher` which events they want to listen to. Then, the `subject` (whatever is doing the work) tells the `dispatcher` to dispatch the event. The `dispatcher` is then responsible for calling the `listener` methods.

You could argue that `pub/sub` better follows the `Single Responsibility Principle`. Battling characters and also registering and calling the observers are two separate responsibilities that we've jammed into `GameApplication`.

**Add the ability to run code before a battle starts by using pub/sub.**

1. **Create an event class.** `Event/FightStartingEvent.php`<br>
    This will be the *object that is passed as an argument* to all of the `listener` methods.

    ```php
    [FightStartingEvent.php]
    class FightStartingEvent
    {
    
    }
    ```

2. **`Dispatch` this event inside of `GameApplication`.**<br>
    Instead of writing our own event `dispatcher`, we're going to use `Symfony's`.
    - Autowire the `EventDispatcherInterface` in `GameApplication` constructor from `Psr\EventDispatcher\EventDispatcherInterface`.
    - In `play()` method dispatch the Event:

        ```php
        [GameApplication.php]
        class GameApplication
        {
            ...
            public function __construct(
                private CharacterBuilderFactory $characterBuilderFactory,
                private EventDispatcherInterface $eventDispatcher
            )
            {
            }

            public function play(Character $player, Character $ai): FightResult
            {
                $this->eventDispatcher->dispatch(new FightStartingEvent());
                ...
            }
            ...   
        }
        ```

3. **Register a listener to this event.**
    - In `GameCommand.php` add to constructor `EventDispatcher` from `Symfony\Component\EventDispatcher\EventDispatcherInterface`:
    - In `GameCommand.php` in `execute()` method add listener:

        ```php
        [GameCommand.php]
        class GameCommand extends Command
        {
            public function __construct(
                private readonly GameApplication $game,
                private readonly EventDispatcherInterface $eventDispatcher,
            )
            {
               parent::__construct();
            }

            protected function execute(InputInterface $input, OutputInterface $output): int
            {
                $io = new SymfonyStyle($input, $output);
                $this->eventDispatcher->addListener(FightStartingEvent::class, function() use ($io){
                    $io->note('Fight is starting...');
                });
                ...
            }
            ...
        }
        ```

## 11. Pub Sub Event Class & Subscribers in Symfony

Pass information to our listener, like who is about to battle.<br>
Plus, we'll see how the event listener system is used in a real Symfony app by leveraging the container to wire everything up.

```php
[GameCommand.php]
class GameCommand extends Command
{
    ...
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        ...
        $this->eventDispatcher->addListener(FightStartingEvent::class, function() use ($io){
            $io->note('Fight is starting...');
        });
        ...
    }
    ...
}
```

**Usually a listener will be a method inside a class.**

1. We need to have a little bit more info in our `listener` function, like who is about to battle.<br>
    That's the job of `FightStartingEvent.php` event class. It can carry whatever data we want.

    ```php
    [FightStartingEvent.php]
    class FightStartingEvent
    {
        public function __construct(public Character $player, public Character $ai)
        {
        }
    }
    ```

2. In `GameApplication.php` we need to pass this data when `dispatch()` the event in `play()` method:
    
    ```php
    [GameApplication.php]
    class GameApplication
    {
        ...
        public function play(Character $player, Character $ai): FightResult
        {
            $this->eventDispatcher->dispatch(new FightStartingEvent($player, $ai));
            ...
        }
    }
    ```

3. In our `listener` (in `GameCommand.php::execute()`), this function will be passed a `FightStartingEvent` object. Now we can say "Fight is starting against", followed by `$event->ai->getNickname()`:
    
    ```php
    [GameCommand.php]
    class GameCommand extends Command
    {
        ...
        protected function execute(InputInterface $input, OutputInterface $output): int
        {
            ...
            $this->eventDispatcher->addListener(FightStartingEvent::class, function(FightStartingEvent $event) use ($io){
                $io->note('Fight is starting against ' . $event->ai->getNickname());
            });
            ...
        }
        ...
    }
    ```

### Creating an Event Subscriber

As easy as this `inline listener` is, it's more common to create a separate class for your `listener`.
- You can either create a `listener` class, which is basically a class that has this code `function(FightStartingEvent $event) use ($io){ $io->note('Fight is starting against ' . $event->ai->getNickname()); });` here as a `public function`.
- Or you can create a class called a `subscriber`.

Both are completely valid ways to use the `pub/sub pattern`. The only difference is how you register a `listener` versus a `subscriber`, which is pretty minor.<br>
Let's refactor to a subscriber because they're easier to set up in Symfony.

1. In the `Event` directory, create a new class `OutputFightStartingSubscriber`, since this `subscriber` is going to output that a battle is beginning:

    ```php
    [OutputFightStartingSubscriber.php]
    class OutputFightStartingSubscriber implements EventSubscriberInterface
    {
        public function onFightStart(FightStartingEvent $event): void
        {
            $io = new SymfonyStyle(new ArrayInput([]), new ConsoleOutput());
            
            $io->note('Fight is starting against ' . $event->ai->getNickname());
        }
    
        public static function getSubscribedEvents()
        {
            return [
                FightStartingEvent::class => 'onFightStart' // method "onFightStart()" in this class will be called.
            ];
        }
    }
    ```

2. In `GameCommand` class in `execute()` method instead of `addListener()`, say `addSubscriber()`, and inside of that, `new OutputFightStartingSubscriber()`:

    ```php
    [GameCommand.php]
    class GameCommand extends Command
    {
        ...
        protected function execute(InputInterface $input, OutputInterface $output): int
        {
            ...
            $this->eventDispatcher->addSubscriber(new OutputFightStartingSubscriber());
            ...
        }
        ...
    }
    ```

**It's working so well, it's outputting twice. We're amazing!**

Why is it printing twice? This is thanks to `auto-configuration`! Whenever you create a class that implements `EventSubscriberInterface`, `Symfony's` container is already taking that and registering it on the `EventDispatcher`.

In other words, `Symfony`, internally, is already calling this line `$this->eventDispatcher->addSubscriber(new OutputFightStartingSubscriber());`. So, we can delete it!

### How do we use the `pub/sub` pattern in Symfony?

- Just create a class, make it implement `EventSubscriberInterface` and... done! Symfony will automatically register it.
- To dispatch an `event`, create a new `event` class and `dispatch` that `event` anywhere in your code.

**Benefits of `pub/sub`: They're really the same as the `observer`, though, in practice, `pub/sub` is a bit more common... probably because Symfony already has this great `event dispatcher`. Half of the work is already done for us!**

## 12. The Decorator Pattern

**This pattern is a `structural pattern`, so it's all about how you organize and connect related classes.**

**Here's the technical definition:**

> The `decorator pattern` allows you to attach new behaviors to objects by placing these objects inside special wrapper objects that contain the behaviors.

**Yeah... Let's try this definition instead:**

> The `decorator pattern` is like an intentional man-in-the-middle attack. You replace a class with your custom implementation, run some code, then call the true method.

*This is a particularly common pattern to leverage if the class you want to modify is a vendor service that... you can't actually change. And especially if that class doesn't give us any other way to hook into it, like by implementing the `observer` or `strategy patterns`.*

### The Goal

**Let's print something onto the screen whenever a player levels up.**

> For the `decorator pattern` to work, there's just one rule: the class that we want to decorate needs to implement an interface.

1. Create new interface `Service/XpCaplculatorInterface.php`:

    ```php
    [XpCaplculatorInterface.php]
    interface XpCalculatorInterface
    {
        public function addXp(Character $winner, int $enemyLevel): void;
    }
    ```

2. Implement this interface to `Service/XpCalculator.php`:

    ```php
    [XpCalculator.php]
    class XpCalculator implements XpCalculatorInterface
    {
    ...
    }
    ```

3. In `Obsrver/XpEarnedObserver.php` change argument type in constructor:

    ```php
    [XpEarnedObserver.php]
    class XpEarnedObserver implements GameObserverInterface
    {
        public function __construct(private readonly XpCalculatorInterface $xpCalculator)
        {
        }
        ...
    }
    ```

4. Create `Decorator` class `Service/OutputtingXpCalculator.php`:

    ```php
    [OutputtingXpCalculator.php]
    class OutputtingXpCalculator implements XpCalculatorInterface
    {
        public function __construct(
            private readonly XpCalculatorInterface $innerCalculator
        )
        {
        }
    
        public function addXp(Character $winner, int $enemyLevel): void
        {
            $beforeLevel = $winner->getLevel();
    
            $this->innerCalculator->addXp($winner, $enemyLevel);
    
            $afterLevel = $winner->getLevel();
    
            if ($afterLevel > $beforeLevel) {
                $output = new ConsoleOutput();
    
                $output->writeln('___________________________');
                $output->writeln('<bg=green;fg=white>Congratulations! You\'ve leveled up!</>');
                $output->writeln(sprintf('You are now level "%d"', $winner->getLevel()));
                $output->writeln('___________________________');
            }
        }
    }
    ```

`Decorator` class is done! But... how do we hook this up? What we need to do is replace all instances of `XpCalculator` in our system with our new `OutputtingXpCalculator`.

1. Manually:
    - Comment out the `subscribe` method call in `Kernel.php`:

        ```php
        [Kernel.php]
        class Kernel extends BaseKernel implements CompilerPassInterface
        {
            ...
            public function process(ContainerBuilder $container)
            {
                ...
                foreach ($taggedObservers as $id => $tags) {
                    //$definition->addMethodCall('subscribe', [new Reference($id)]);
                }
            }
        }
        ```
    - In `GameCommand.php` put back manual `observer pattern` setup logic:
        ```php
        [GameCommand.php]
        class GameCommand extends Command
        {
            ...
            protected function execute(InputInterface $input, OutputInterface $output): int
            {
                $xpCalculator = new XPCalculator();
                $this->game->subscribe(new XpEarnedObserver($xpCalculator));
                
                ...
            }
            ...
        }
        ```

    - In `GameCommand` instantiate `OutputtingXpCalculator` and pass `XpCalculator` to it:

        ```php
        [GameCommand.php]
        class GameCommand extends Command
        {
            ...
            protected function execute(InputInterface $input, OutputInterface $output): int
            {
                $xpCalculator = new XPCalculator();
                $xpCalculator = new OutputtingXpCalculator($xpCalculator);
                $this->game->subscribe(new XpEarnedObserver($xpCalculator));
                
                ...
            }
            ...
        }
        ```

## 13. Decoration with Symfony's Container

What we really want is for `XpEarnedObserver` to autowire `XpCalculatorInterface` like normal, without us needing to do any of this manual instantiation.<br>
But we need the `container` to pass it our `OutputtingXpCalculator` decorator service, not the original `XpCalculator`.

1. Undo manual code in `GameCommand.php`:

    ```php
    [GameCommand.php]
    class GameCommand extends Command
    {
        ...
        protected function execute(InputInterface $input, OutputInterface $output): int
        {
            $io = new SymfonyStyle($input, $output);
            ...
        }
        ...
    }
    ```

2. Uncomment code in `Kernel.php` that we commented earlier:

    ```php
    [Kernel.php]
    class Kernel extends BaseKernel implements CompilerPassInterface
    {
        ...
        public function process(ContainerBuilder $container)
        {
            ...
            foreach ($taggedObservers as $id => $tags) {
                $definition->addMethodCall('subscribe', [new Reference($id)]);
            }
        }
    }
    ```

3. We autowire `XpCalculatorInterface` interface in `XpEarnedObserver` class, but Symfony doesn't know which service we need to use `App\Service\OutputtingXpCalculator` or `App\Service\XpCalculator`.
    - In `services.yaml` config add the code:

        ```yaml
        [services.yaml]
        services:
            ...
            App\Service\XpCalculatorInterface: '@App\Service\OutputtingXpCalculator'
        ```

4. Now we have *Circular reference* because Symfony is autowiring `OutputtingXpCalculator` into `XpEarnedObserver` AND it's also autowiring `OutputtingXpCalculator` into itself:
    - Configure `OutputtingXpCalculator` service manually in `services.yaml`:

        ```yaml
        [services.yaml]
        services:
            ...
            App\Service\XpCalculatorInterface: '@App\Service\OutputtingXpCalculator'
          
            App\Service\OutputtingXpCalculator:
                arguments:
                    $innerCalculator: '@App\Service\XpCalculator'
        ```

        This will override the argument for just this one case.

## 14. Decoration: Override Core Services & AsDecorator

let's extend the behavior of Symfony's core `EventDispatcher` service so that whenever an event is dispatched, we dump a debugging message.

1. Create new class `Decorator/DebugEventDispatcherDecorator.php`, implement a `EventDispatcherInterface` from `Symfony/Component`.

    ```php
    [DebugEventDispatcherDecorator.php]
    class DebugEventDispatcherDecorator implements EventDispatcherInterface
    {
        public function __construct(private readonly EventDispatcherInterface $eventDispatcher)
        {
        }
    
        public function dispatch(object $event, string $eventName = null): object
        {
            dump('___________________');
            dump('Dispatching event: ' . $event::class);
            dump('___________________');
    
            return $this->eventDispatcher->dispatch($event, $eventName);
        }
    
        public function addListener(string $eventName, callable|array $listener, int $priority = 0): void
        {
            $this->eventDispatcher->addListener($eventName, $listener, $priority);
        }
    
        public function addSubscriber(EventSubscriberInterface $subscriber): void
        {
            $this->eventDispatcher->addSubscriber($subscriber);
        }
    
        public function removeListener(string $eventName, callable $listener): void
        {
            $this->eventDispatcher->removeListener($eventName, $listener);
        }
    
        public function removeSubscriber(EventSubscriberInterface $subscriber): void
        {
            $this->eventDispatcher->removeSubscriber($subscriber);
        }
    
        public function getListeners(string $eventName = null): array
        {
            $this->eventDispatcher->getListeners($eventName);
        }
    
        public function getListenerPriority(string $eventName, callable $listener): ?int
        {
            return $this->eventDispatcher->getListenerPriority($eventName, $listener);
        }
    
        public function hasListeners(string $eventName = null): bool
        {
            return $this->eventDispatcher->hasListeners($eventName);
        }
    }
    ```

    Our decorator class is done! But, there are many places in Symfony that rely on the service whose ID is `event_dispatcher`. So here's the million dollar question: how can we replace that service with our own service... but still get the original event dispatcher passed to us?

    - Symfony has a feature built specifically for this! Go to the top of our `decorator` class, add a PHP 8 attribute called: `#[AsDecorator()]` and pass the ID of the service that we want to decorate: `event_dispatcher`:
    
        ```php
        [DebugEventDispatcherDecorator.php]
        #[AsDecorator('event_dispatcher')]
        class DebugEventDispatcherDecorator implements EventDispatcherInterface
        {
            ...
        }
        ```

### Using AsDecorator with `OutputtingXpCalculator`

1. In `services.yaml` remove manually added arguments and change the interface to point to the original, undecorated service: `XpCalculator`:

    ```yaml
    [services.yaml]
    services:
        ...
        App\Service\XpCalculatorInterface: '@App\Service\XpCalculator'
    ```

2. If we tried our app now, it would work, but it wouldn't be using our decorator. But now, go into `OutputtingXpCalculator` add `#[AsDecorator()]` and pass it `XpCalculatorInterface::class`, since that's the ID of the service we want to replace:

    ```php
    [OutputtingXpCalculator.php]
    #[AsDecorator(XpCalculatorInterface::class)]
    class OutputtingXpCalculator implements XpCalculatorInterface
    {
        ...
    }
    ```

### Decoration in the Wild?

Where do we see decoration in the wild? The answer to that is... sort of all over! In API Platform, it's common to use decoration to extend core services like the `ContextBuilder`.<br>
And Symfony itself uses decoration pretty commonly to add `debugging features` while we're in the `dev` environment.<br>
For example, we know that this `EventDispatcher` class would be used in the `prod` environment. But in the `dev` environment - I'll hit to search for a `TraceableEventDispatcher` - assuming that you have some debugging tools installed, this is the actual class that represents the `event_dispatcher` service. It decorates the real one!

### Problems Solved by Decorator

- It allows us to extend the behavior of an existing class - like `XpCalculator` - even if that class does not contain any other extension points.<br>
  This means we can use it to `override` vendor services when all else fails. 
- The only downside to the `decorator pattern` is that we can only run code **before** or **after** the core method.<br>
  And the service we want to decorate **must** implement an `interface`.